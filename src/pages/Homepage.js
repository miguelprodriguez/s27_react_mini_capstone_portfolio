import React from 'react';

import LandingPage from '../components/LandingPage';
import AboutMe from '../components/AboutMe';
import Tools from '../components/Tools';
import ProjectCards from '../components/ProjectCards';
import Contact from '../components/Contact';

export default function Homepage(){
	return(
		<>
			<LandingPage />
			<AboutMe />
			<Tools />
			<ProjectCards />
			<Contact />
		</>
		)
}