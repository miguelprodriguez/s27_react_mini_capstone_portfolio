import project1 from '../card1.png'
import project2 from '../card2.png'
import project3 from '../card3.png'
import project4 from '../card4.png'
import project5 from '../card5.png'

const projects = [
    {
        title: 'Dev Portfolio',
        image: project1,
        tools: 'HTML, CSS, Bootstrap, AOS',
        description: 'A simple personal portfolio page with animations',
        button:
            <a href='https://miguelprodriguez.gitlab.io/capstone-1/#' target="_blank">
                <button className="mt-0">View here</button>
            </a>
    },
    {
        title: 'Course Booking',
        image: project2,
        tools: 'MongoDB, ExpressJS, NodeJS',
        description: 'My dynamic app where users can enroll to courses put by admin.',
        button:
            <a href='https://miguelprodriguez.gitlab.io/cs_2_public_client/' target="_blank">
                <button className="mt-0">View here</button>
            </a>
    },
    {
        title: 'Budget Tracker',
        image: project3,
        tools: 'MongoDB, ExpressJS, NextJS, NodeJS, Google Login',
        description: 'App where users can log their daily expenses and income.',
        button:
            <a href='https://csp3-client.vercel.app/' target="_blank">
                <button className="mt-0">View here</button>
            </a>
    },
    {
        title: 'Graphic Artist Portfolio',
        image: project4,
        tools: 'ReactJS, GoDaddy, cPanel, React Router DOM (In-progress)',
        description: 'A project wherein I made an online portfolio for a graphic designer.Pending artworks and descriptions from the artist.',
        button:
            <a href='http://krystianmillora.com/' target="_blank">
                <button className="mt-0">View here</button>
            </a>
    },
    {
        title: 'Todo List',
        image: project5,
        tools: 'ReactJS, Redux, Material UI, Cypress (In-progress)',
        description: ' Additional project for me to understand the state management using Redux',
        button:
            <a href=''>
                <button className="mt-0" disabled>View here</button>
            </a>
    }
]

export default projects