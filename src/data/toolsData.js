import React from 'react'
// Icons
import { AiFillHtml5 } from 'react-icons/ai';
import { DiCss3 } from 'react-icons/di';
import { SiJavascript } from 'react-icons/si';
import { BsBootstrapFill } from 'react-icons/bs';
import { DiGit } from 'react-icons/di';
import { DiMongodb } from 'react-icons/di';
import { FaReact } from 'react-icons/fa';
import { FaNodeJs } from 'react-icons/fa';
import { SiGitlab } from 'react-icons/si';
import { SiRedux } from 'react-icons/si';
import { SiVisualstudiocode } from 'react-icons/si';

const tools = [
    {
        icon: <AiFillHtml5 />,
        name: 'HTML'
    },
    {
        icon: <DiCss3 />,
        name: 'CSS'
    },
    {
        icon: <SiJavascript />,
        name: 'JavaScript'
    },
    {
        icon: <BsBootstrapFill />,
        name: 'Bootstrap'
    },
    {
        icon: <DiMongodb />,
        name: 'MongoDB'
    },
    {
        icon: <h1>EX</h1>,
        name: 'Express'
    },
    {
        icon: <FaReact />,
        name: 'React'
    },
    {
        icon: <FaNodeJs />,
        name: 'NodeJS'
    },
    {
        icon: <DiGit />,
        name: 'Git'
    },
    {
        icon: <SiGitlab />,
        name: 'GitLab'
    },
    {
        icon: <SiRedux />,
        name: 'Redux'
    },
    {
        icon: <SiVisualstudiocode />,
        name: 'VS Code'
    },
]

export default tools