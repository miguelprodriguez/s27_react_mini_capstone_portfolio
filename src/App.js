import React from 'react';

// Pages
import Homepage from './pages/Homepage';
import NavBar from './components/NavBar';
// Animations & CSS
import './App.css';

// React-links
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

export default function App() {
  return (
    <div>
      <Router>
      <NavBar />
          <Switch> {/* the route component renders components within this container based on the defined routes that we will assign to them.*/}
            <Route exact path = "/" component = {Homepage} />
          </Switch>
      </Router>
    </div>
  )
}


