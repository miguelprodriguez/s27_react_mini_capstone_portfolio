import React from 'react'

// Bootstrap
import { Container } from 'react-bootstrap'
import { Row } from 'react-bootstrap'

import data from '../data/projectsData'
import ProjectCardItem from './ProjectCardItem'

export default function ProjectCards() {
	const projects = data.map((data, index) => {
		return <ProjectCardItem  
			title={data.title}
			image={data.image}
			tools={data.tools}
			description={data.description}
			button={data.button}
			key={index}
		/>
	})
	
	return (
		<>
			<Container className="background-white py-5" fluid id="ProjectCards">
				<Row className="text-center">
					{projects}
				</Row>

			</Container>
		</>
	)
}