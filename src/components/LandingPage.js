import React from 'react';
import { Container } from 'react-bootstrap';

// Animations 
import Typewriter from 'typewriter-effect';
import { NavHashLink } from 'react-router-hash-link';

// Icons
import { BsChevronDoubleDown } from 'react-icons/bs';

export default function LandinPage(){
	return(
		<>
		<Container className="background_img text-white" fluid>
			<div>
				<h1 className="text-center-vertical">
					<strong><Typewriter  options = {{
					    strings: ['Hello World!', 'I am Miguel Rodriguez', 'A web developer'],
					    autoStart: true,
					    loop: true,
					  }}	
					/>
					</strong>
				</h1>
			</div>

			<div className="text-center-bottom">
				<NavHashLink className="links" smooth to="#AboutMe">
					<h3 id="selected"><BsChevronDoubleDown /></h3> 
				</NavHashLink>
			</div>
		</Container>


		</>
		)
}