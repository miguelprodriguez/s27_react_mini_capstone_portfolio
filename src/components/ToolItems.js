import React from 'react'
import { Col } from 'react-bootstrap';

export default function ToolsComponent({icon, name}) {
    return (
        <Col xs={3} className="links">
            <h1 >{icon}</h1>
            <p>{name}</p>
        </Col>
    )
}
