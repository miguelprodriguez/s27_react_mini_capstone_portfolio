import React, { useState } from 'react';

// we need to import the necessary Bootstrap components in order for us to utilize the pre-setted Bootstrap
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

// React Router with Bootstrap without reloading
// import { NavLink } from 'react-router-dom'
// <Nav.Link as={HashLink} exact to="/aboutme" activeClassName="selected">About Me</Nav.Link>

import { NavHashLink } from 'react-router-hash-link';

export default function NavBar(){

	const [ navColor, setNavColor ] = useState(true);
	const [ linkHome, setLinkHome ] = useState(false);
	const [ linkAboutMe, setLinkAboutMe ] = useState(false);
	const [ linkProject, setProject ] = useState(false);
	const [ linkContact, setContact ] = useState(false);

	// ES6 
	// const changeBackground = () => {
	function changeBackground() {
		// check y-xis
		// console.log(window.scrollY)
		if (window.scrollY >= 280 ) {
			setNavColor(false);
		}
		else {
			setNavColor(true);
		}
	}

	// Links
	function changeHome() {
		if (window.scrollY < 650) {
			setLinkHome(true);
		}
		else {
			setLinkHome(false);
		}
	}

	function changeAboutMe() {
		if (window.scrollY > 650 && window.scrollY <= 1150) {
			setLinkAboutMe(true);
		}
		else {
			setLinkAboutMe(false);
		}
	}

	function changeProject() {
		if (window.scrollY > 1150 && window.scrollY <= 1850) {
			setProject(true);
		}
		else {
			setProject(false);
		}
	}

	function changeContact() {
		if (window.scrollY > 1850) {
			setContact(true);
		}
		else {
			setContact(false);
		}
	}

	// console.log to check every value
	window.addEventListener('scroll', changeBackground)
	window.addEventListener('scroll', changeHome)
	window.addEventListener('scroll', changeAboutMe)
	window.addEventListener('scroll', changeProject)
	window.addEventListener('scroll', changeContact)

	return(
		<>
			<Navbar collapseOnSelect variant="dark" expand="lg" sticky="top" className={navColor ? "background-image" : "background-teal" }>
				<Navbar.Brand href="#"></Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mx-auto">
						<NavHashLink className="no-underline" smooth to="#top">
							<Nav.Link eventKey="1" id={linkHome ? "selected-current" : "selected"}> Home </Nav.Link>
						</NavHashLink>
  						
						<NavHashLink className="no-underline" smooth to="#AboutMe">
							<Nav.Link eventKey="2" id={linkAboutMe ? "selected-current" : "selected"}> About Me </Nav.Link>
						</NavHashLink>
						
						<NavHashLink className="no-underline" smooth to="#Projects">
							<Nav.Link eventKey="3" id={linkProject ? "selected-current" : "selected"}> Projects </Nav.Link>
						</NavHashLink>
						
						<NavHashLink className="no-underline"  smooth to="#Contact">
							<Nav.Link eventKey="" id={linkContact ? "selected-current" : "selected"}> Contact Me </Nav.Link>	 	
						</NavHashLink>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		</>
		)
}

