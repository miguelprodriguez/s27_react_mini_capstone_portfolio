import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { HashLink } from 'react-router-hash-link';

export default function AboutMe(){
	return (
		<>
			<Container className="py-5" id="AboutMe" fluid>
				<Container className="my-5">
					<Row>
						<Col md={4}>
							<Container className="text-center">
								<br />
								<img src="download.png" alt="" className="img-fluid center text-center-img" /> 
							</Container>
						</Col>
						<Col  md={8} className="my-5" id="aboutMe-description">
							<h1 >About Me</h1>
							<p>Hello world!</p>
							<p> I am <span><strong>Miguel Rodriguez</strong></span>, a Bachelor in Banking and Finance graduate from Polytechnic University of the Philippines. I am also a full-stack web development course completer at Zuitt Coding Bootcamp. Please take the time to look at my projects below.</p>
							<a href="https://drive.google.com/file/d/1A69EnvHIDphOewE0b97qWk9_hFvetWj1/view?usp=sharing" target="_blank" rel="noopener noreferrer">
									<button className="button-cv mr-5"> See my CV </button>
							</a>
							<HashLink className="selected" smooth to="#ProjectCards" style = {{ color: "white" }}>
									<button className="button-projects"> See my Projects </button>
							</HashLink>
						</Col>
					</Row>
				</Container>
			</Container>
		</>
		)
}