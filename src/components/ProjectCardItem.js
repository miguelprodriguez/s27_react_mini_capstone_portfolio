import React from 'react'
import { Col } from 'react-bootstrap';

export default function ProjectCardItem({title, image, tools, description, button}) {
    return (
        <Col lg={6} className="my-3" style={{ margin: 'auto' }}>
            <div className="wrapper my-3">
                <img className="image" src={image} />
                <div className="description">
                    <div className="description-positioning">
                        <h2 className="mt-4">{title}</h2>
                        <p>{tools}</p>
                        <p className="text-center-description">
                           {description}
                        </p>
                        {button}
                    </div>
                </div>
            </div>
        </Col>
    )
}
