import React from 'react';
import { Container } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

// Icons
import { AiOutlineGitlab } from 'react-icons/ai';
import { AiOutlineLinkedin } from 'react-icons/ai';
import { SiFreecodecamp } from 'react-icons/si';

export default function ContactStatement(){
	return (
		<div className="background-black py-5" id="Contact">
			<Container className="text-center my-5" style={{ color: "black" }}>
				<Jumbotron>
					<h1>
						Let's Make Something Great 
					</h1>
					<h5>
						<a href="mailto:mglprodriguez@gmail.com" className="links" style={{ color: "black" }}>mglprodriguez@gmail.com</a>
					</h5>
				</Jumbotron>
			</Container>
			
			<h3 className="text-center text-white">
				Get in touch with me on
			</h3>

		<Container className="text-center">
			<Row>
				<Col xs={4} className="black" >
					<a href="https://gitlab.com/miguelprodriguez" target="_blank" style={{ color: "white" }} className="links">
					<h1 className="links"><AiOutlineGitlab /></h1>
					</a>
				</Col>
				<Col xs={4}>
					<a href="https://www.linkedin.com/in/miguelprodriguez/" target="_blank" style={{ color: "white" }} className="links">
					<h1 className="links"><AiOutlineLinkedin /></h1>
					</a>
				</Col>
				<Col xs={4}>
					<a href="https://www.freecodecamp.org/fcc512e3891-8bd0-486e-b4e1-38ec72cd9855" target="_blank" style={{ color: "white" }}>
					<h1 className="links"><SiFreecodecamp /></h1>
					</a>
				</Col>
			</Row>
		</Container>

		</div>
		)
}