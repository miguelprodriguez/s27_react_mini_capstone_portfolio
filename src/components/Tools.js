import React from 'react'
import Container from 'react-bootstrap/Container'
import { Row, Col } from 'react-bootstrap'
import ToolItems from './ToolItems'
import toolsData from '../data/toolsData'

export default function Projects() {
	const allTools = toolsData.map((tool, index) => {
		return <ToolItems icon={tool.icon} name={tool.name} key={index} />
	})
	return (
		<>
			<Container className="py-5 background-black text-center text-white" fluid id="Projects">
				<h3 className="my-5 text-center">
					Tools I have used so far to make the projects below possible
				</h3>
				<Row>
					{allTools}
				</Row>
			</Container>

		</>
	)
}